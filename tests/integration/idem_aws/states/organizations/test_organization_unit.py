import uuid

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization_unit(hub, ctx, aws_organization):
    name = "idem-test-organization-unit-" + str(uuid.uuid4())

    roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    root_id = None

    if roots_resp:
        root_id = roots_resp["ret"]["Roots"][0]["Id"]

    create_tag = [
        {"Key": "org", "Value": "idem"},
        {"Key": "env", "Value": "test"},
        {"Key": "state", "Value": "temp"},
    ]
    # create organization unit
    create_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx, name=name, org_unit_name=name, parent_id=root_id, tags=create_tag
    )

    created_org_id = create_ret.get("new_state")["Id"]

    assert create_ret["result"], create_ret["comment"]
    assert not create_ret.get("old_state")

    assert create_ret.get("new_state")
    new_state = create_ret.get("new_state")
    assert new_state
    assert dict(new_state["Tags"]) == dict(
        create_tag
    ), "tags used in the create should match new state"

    # update organization unit

    updated_name = "idem-test-organization-unit-" + str(uuid.uuid4())
    update_tag = [
        {"Key": "org", "Value": "idem-aws"},
        {"Key": "env", "Value": "sandbox"},
    ]
    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=created_org_id,
        org_unit_name=updated_name,
        parent_id=root_id,
        tags=update_tag,
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")
    updated_state = update_ret.get("new_state")
    assert updated_state
    assert dict(updated_state["Tags"]) == dict(
        update_tag
    ), "tags used in the update should match updated state"
    assert (
        updated_state["Name"] == updated_name
    ), "returned ou name should get updated ou name"
    assert (
        updated_state["Id"] == created_org_id
    ), "returned Id should match the state's identifier"

    old_state = update_ret.get("old_state")

    assert dict(old_state["Tags"]) == dict(
        new_state["Tags"]
    ), "tags used in the update should match updated state"
    assert (
        old_state["Name"] == new_state["Name"]
    ), "returned ou name should get updated ou name"
    assert (
        old_state["Id"] == new_state["Id"]
    ), "returned Id should match the state's identifier"

    # describe organizational unit

    describe_ret = await hub.states.aws.organizations.organization_unit.describe(ctx)

    assert updated_state["Id"] in describe_ret
    describe_params = describe_ret[old_state["Id"]].get(
        "aws.organizations.organization_unit.present"
    )
    assert describe_params[0].get("org_unit_name") == updated_state["Name"]
    assert root_id == describe_params[1].get("parent_id")
    assert dict(update_tag) == dict(describe_params[2].get("tags"))
    # delete organizational unit
    delete_ret = await hub.states.aws.organizations.organization_unit.absent(
        ctx, name=updated_state["Id"]
    )

    if not hub.tool.utils.is_running_localstack(ctx):
        assert not delete_ret.get("new_state"), delete_ret.get("old_state")

        delete_ret = await hub.states.aws.organizations.organization_unit.absent(
            ctx, name=updated_state["Id"]
        )

        assert (
            f"aws.organizations.organization_unit {updated_state['Id']} already absent"
            == delete_ret["comment"]
        )


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization_unit_update(
    hub, ctx, aws_organization, aws_organization_unit
):
    describe_ret = await hub.states.aws.organizations.organization_unit.describe(ctx)

    ou = describe_ret.get(aws_organization_unit["Id"]).get(
        "aws.organizations.organization_unit.present"
    )
    random = "idem-test-organization-unit-random-" + str(uuid.uuid4())
    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=aws_organization_unit["Id"],
        org_unit_name=aws_organization_unit["Name"],
        parent_id=ou[1]["parent_id"],
        tags=aws_organization_unit["Tags"],
    )

    assert (
        update_ret["new_state"] == aws_organization_unit
    ), "Can't have two OU with same org_unit_name,hence no update"

    updated_name = "idem-test-ou-" + str(uuid.uuid4())
    update_tag = [
        {"Key": "org", "Value": "idem-aws"},
        {"Key": "env", "Value": "sandbox"},
    ]

    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=random,
        org_unit_name=updated_name,
        parent_id=ou[1]["parent_id"],
        tags=update_tag,
    )

    assert (
        update_ret["new_state"] != aws_organization_unit
    ), "state changes since org_unit_name and tag was updated "
