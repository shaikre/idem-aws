from unittest import mock

import botocore.session
import pytest
from dict_tools import data


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture
def ctx():
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        acct=data.NamespaceDict(
            provider_tag_key="provider_tag_key",
            session=mock.create_autospec(botocore.session.Session),
            endpoint_url="https://fake_endpoint_url.com:4566",
        )
    )
